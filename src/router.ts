import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
import Home from './views/Home.vue';

import login from './views/Login.vue';
import signup from './views/Signup.vue';
import clientView from './views/ClientView.vue';
import cart from './views/Cart.vue';
import book from './views/Book.vue';
import changePassword from './views/ChangePassword.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
      path: '/user/login',
      name: 'login',
      component: login
  },
  {
      path: '/user/signup',
      name: 'signup',
      component: signup
  },
  {
      path: '/client/:email',
      name: 'clientView',
      component: clientView
  },
  {
      path: '/client/:email/cart',
      name: 'cart',
      component: cart
  },
  {
      path: '/books',
      name: 'book',
      component: book
  },
  {
      path: '/client/:email/change-password',
      name: 'changePassword',
      component: changePassword
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

export default router
